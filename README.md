# Repositorios Territoriais

Projeto destinado para organização de conteúdos, referências e projeto sobre o quarto distrito; Todos os materiais referêntes ao projeto podem ser acessados no [drive do projeto](https://drive.google.com/drive/folders/1cbzkLujWam0HpJF9zBVGxv5JzhfdG9qk)

O projeto está organizado em três grupos, colaboradores, apoiadores e mantenedores.

Este repositório foi criado e é mantido colaborativamente com o objetivo de publicizar trabalhos que fomentam a discussão sobre o território do quarto distrito de Porto Alegre. Os conteúdos aqui vinculados não são de responsabilidade dos mantenedores, não ha qualquer vinculo partidário ou interesse econômico na concepção dessa plataforma. São vinculados trabalhos de licença livre, acesso público ou com o consentimento dos autores. Caso haja algum trabalho que vinculado erroneamente ou algum autor que queira vincular um novo trabalho por favor entre em contato.
